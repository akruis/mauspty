#include	<sys/types.h>
#include	<signal.h>
#include        <sys/time.h>
#include        <sys/ioctl.h>
#include        <fcntl.h>
#include	<errno.h>
#include        <time.h>
#include	"ourhdr.h"

#define	BUFFSIZE	512
/* #define DEBUG_LOOP 1 */

#ifndef TEMP_FAILURE_RETRY
#define TEMP_FAILURE_RETRY(sc) ({ int s_tmp; \
  do { s_tmp = sc; } while ((s_tmp == -1) && (EINTR == errno)); s_tmp; })
#endif

volatile sig_atomic_t got_sigchld=0;

void sig_chld(int signr )
{
  got_sigchld = 1;
}



void loop(int ptym, int ignoreeof, int carrierwatch, int timeout, int pty_packet_mode)
{
  fd_set rfds;         /* File Handles f�r select (read) */
  fd_set wfds;         /* File Handles f�r select (write) */
  fd_set efds;         /* File Handles f�r select (exeption) */
  
  struct timeval tv;   /* Wartezeit */
  int maxfd = max(max(STDIN_FILENO,STDOUT_FILENO),ptym) +1;/* H�chster Deskriptor + 1 */
  int retval;          /* Ergebnis von select */

  char buff1[BUFFSIZE]; /* von STDIN zum PTY-Slave */
  char buff2[BUFFSIZE]; /* vom PTY-Slave zu STDOUT */

  char * p1; /* Datenanfang von buff1 */
  char * p2; /* Datenanfang von buff2 */

  size_t l1; /* Anzahl der Bytes in buff1 */
  size_t l2; /* Anzahl der Bytes in buff2 */

  int  want_read_stdin;  /* Flag, das innerhalb der Hauptschleife anzeigt, 
                            das von der Standard-Eingabe gelesen werden sollte */
  int  want_read_ptym;   /* Dito f�r das Pseudo TTY */
  
  int  stdin_at_eof=0; /* wird auf 1 gesetzt, wenn EOF von STDIN */
  int  ptym_at_eof =0; /* wird auf 1 gesetzt, wenn EOF von ptym */
  int  carrier_is_lost =0; /* wird bei einem Carrier lost auf 1 gesetzt */
  int  carrierfd=STDIN_FILENO; /* Filedescriptor, mit dem der Carriertest durchgef�hrt wird */
  int  got_sigchld_flag = 0;   /* Flag, das sich merkt, wenn ein SIG_CHLD eingetreten ist */
  time_t start_time=time(NULL);

  int  one=1; /* dummy */
  int  ret;   /* tmp-value */

  /* Buffer initialisieren */
  p1=buff1; p2=buff2;
  l1=l2=0;

  errno=0;

  /* Filehandles auf nicht blockierend schalten */
  set_fl(STDIN_FILENO,O_NONBLOCK);
  set_fl(STDOUT_FILENO,O_NONBLOCK);
  set_fl(ptym,O_NONBLOCK);

  /* pty in den Packet-Mode schalten */
  if(pty_packet_mode && isatty(ptym)) {
    if(ioctl(ptym,TIOCPKT,&one))
      err_sys("error: switching pty to packet mode");
    pty_packet_mode = 1; /* PTY is now in packet mode */
  } else 
    pty_packet_mode = 0;

  /* Hauptschleife */
  while(l1 || l2 || (!ptym_at_eof && (!stdin_at_eof || ignoreeof)))
    {
#ifdef DEBUG_LOOP
      perror("loop start"); fprintf(stderr, "l1=%d, l2=%d\n", (int)l1, (int)l2); fflush(stderr);
#endif            
      /* Time out */
      if(timeout > 0 && time(NULL) > timeout + start_time)
	{
#ifdef DEBUG_LOOP
          perror("time out");fflush(stderr);
#endif	
	  if(isatty(STDOUT_FILENO) && -1 == tcflush(STDOUT_FILENO,TCOFLUSH))
	    err_sys("error flushing STDOUT (timeout)");
	  strcpy(buff2,"\r\n\007\007\007Time Out\r\n");
	  p2=buff2;
	  l2=strlen(buff2);
	  ptym_at_eof=1;
	}
	         
      /* -------- Select vorbereiten ------- */
      /* wir verwnden einen Timeout von 5 Sekunden um gelegentlich 
         nach dem Carrier zu sehen */ 
      tv.tv_sec = (got_sigchld || carrier_is_lost) ? 0:5;
      tv.tv_usec = 0;
      /* Wir warten auf die Verf�gbarkeit von Daten zum Lesen, Schreiben und 
         Exeption */
      FD_ZERO(&rfds);      FD_ZERO(&wfds);      FD_ZERO(&efds);
      /* Lesen von stdin und pty-slave */
      if((want_read_stdin=(l1<=0 && !stdin_at_eof))) /* Hier sollte eine Low-Water Mark verwendung finden */
	FD_SET(STDIN_FILENO,&rfds); 
      if((want_read_ptym = (l2<=0 && !ptym_at_eof)))
	FD_SET(ptym,&rfds);
      /* Schreiben auf stdout und pty-slave */
      if(l2>0)
	FD_SET(STDOUT_FILENO,&wfds); 
      if(l1>0) {
	if(!ptym_at_eof) /* Hat es noch Sinn zu schreiben ? */
	  FD_SET(ptym,&wfds);
	else /* nein, dann Buffer verwerfen */
	  l1=0;
      }
      /* Exeption Data vom PTY */
      if(!ptym_at_eof)
	FD_SET(ptym,&efds);

      /* Select Auswahl */
      retval = select(maxfd,&rfds,&wfds,&efds,&tv);
      /* Fehlerbehandlung f�r Select */
      if(-1 == retval && EINTR == errno)
	continue;

      /* Carrier Check */
      if(carrierwatch)
	{
	  int data;
	  if(-1==ioctl(carrierfd,TIOCMGET,&data))
	    perror("TIOCMGET Fehler");
	  if( !(data & TIOCM_CAR))
	    {
	      carrier_is_lost = 1;
#ifdef DEBUG_LOOP
	      perror("Carrier lost");fflush(stderr);
#endif	      
	    }
	}
      /* Test ob child noch lebt. Dies machen wir,
	     da das Child tot sein kann, aber die Files an Enkel-Prozesse
	     vererbt sein k�nnen */
      got_sigchld_flag |= got_sigchld;

      /* Auf Fehler testen */
      if(-1 == retval)
	err_sys("select error");
      /* test, ob irgendeine Aktion ansteht */
      if(retval > 0)
	{
	  char ex;  /* exeption byte */
	  /* --- Ja, es ist was los ----*/
	  
	  /* Test, ob Lesen des PTY m�glich */
	  if((FD_ISSET(ptym,&rfds) || FD_ISSET(ptym,&efds)) && !ptym_at_eof)
	    {
	      if(pty_packet_mode) 
	      {
	      char c;
	      /* lesen:  */
	      if(!l2)
		p2=buff2+1;
	      c=p2[l2-1];
	      ret=TEMP_FAILURE_RETRY(read(ptym,p2+l2-1,
					  sizeof(buff2)-(p2-buff2)-l2));
#ifdef DEBUG_LOOP
	      if(-1 == ret)
		{
		  int en=errno;
		  perror("pty-read");fflush(stderr); 
		  errno=en;
		}
#endif
	      if(-1==ret && errno == EAGAIN)
		ret = 0;
	      else if(ret<=0)
		{ /* eof */
		  ptym_at_eof = 1;
		}
	      if(ret>0)
		{
		  /* Test, ob exeption gekommen */
		  ex = p2[l2-1];
		  p2[l2-1]=c;
		  if(ex) /* da ist doch noch eine exeption gekommen */
		    {
		      ret = 0;
#ifdef DEBUG_LOOP
		      fprintf(stderr,"Exeption: %d\n",(int)ex);fflush(stderr);
#endif
		      if (ex & TIOCPKT_FLUSHREAD) {
#ifdef DEBUG_LOOP
		          fprintf(stderr,"Exeption TIOCPKT_FLUSHREAD\n");fflush(stderr);
#endif
			  /* Inputbuffer leeren */
			  l1=0;
			  if(!stdin_at_eof && isatty(STDIN_FILENO) && 
			     -1 == tcflush(STDIN_FILENO,TCIFLUSH))
			    err_sys("error: flushing pty input queue");
                      }
                      if (ex & TIOCPKT_FLUSHWRITE) {
                        char c;
                        ret=TEMP_FAILURE_RETRY(read(ptym,&c,0));
                        c = (ret == 0 || errno!=EIO);
#ifdef DEBUG_LOOP
                        fprintf(stderr,"Exeption TIOCPKT_FLUSHWRITE (valid: %d)\n", (int)c);fflush(stderr);
#endif
                          if (c) {
                            l2=0;
                            if(isatty(STDOUT_FILENO) && -1 == tcflush(STDOUT_FILENO,TCOFLUSH))
                              err_sys("error: flushing pty output queue");
                          }
  		      }
		      continue;
		    }
		  l2+=ret-1;
		}
	      } /* if(pty_packet_mode) */
	      else
		{ /* normal mode */
		  /* lesen:  */
		  if(!l2)
		    p2=buff2;

		  ret=TEMP_FAILURE_RETRY(read(ptym,p2+l2,
					      sizeof(buff2)-(p2-buff2)-l2));
		  if(-1==ret && errno == EAGAIN)
		    ret = 0;
		  else if(ret<=0)
		    { /* eof */
		      ptym_at_eof = 1;
		      ret = 0;
		    }
		  l2+=ret;
		}
	    }
	    
	  /* Lesen von der Standardeingabe */  
	  if(FD_ISSET(STDIN_FILENO,&rfds) && !stdin_at_eof)
	    {
	      /* lesen */
	      if(!l1) p1=buff1;
	      
	      ret=TEMP_FAILURE_RETRY(read(STDIN_FILENO,p1+l1,
					  sizeof(buff1)-(p1-buff1)-l1));
#ifdef DEBUG_LOOP
	      if(-1 == ret)
		{
		  int en=errno;
		  perror("stdin-read");fflush(stderr);
		  errno =en;
		}
#endif
	      if(-1 == ret && errno == EAGAIN)
		ret = 0;
	      else if(ret <= 0)
		{/* eof */
		  stdin_at_eof=1;
		}  
	      if(ret > 0)
		l1+=ret;
	    }

	  /* Test, ob Schreiben m�glich */
	  if(FD_ISSET(ptym,&wfds) && l1)
	    {
	      if(!ptym_at_eof && !got_sigchld) {
	      /* schreiben */
	      ret=TEMP_FAILURE_RETRY(write(ptym,p1,l1));
	      if(-1 == ret && errno == EAGAIN)
		ret = 0;
	      if(-1 == ret)
		err_sys("write error on pty");
	      p1+=ret;
	      l1-=ret;
	      /* Buffer verschieben */
	      } 
	      else {
		l1 = 0;
	      }
	    }
	  if(FD_ISSET(STDOUT_FILENO,&wfds) && l2)
	    {
	      if(!carrier_is_lost) {
	      /* schreiben */
	      ret=TEMP_FAILURE_RETRY(write(STDOUT_FILENO,p2,l2));
	      if(-1 == ret && errno == EAGAIN)
		ret = 0;
	      if(-1 == ret)
		err_sys("write error on STDOUT");
	      p2+=ret;
	      l2-=ret;
	      /* Buffer verschieben */
	      }
	      else {
		l2 =0;
	      }
	    }
	} /* if(retval > 0)*/
        
        /* Test, ob Lesen vom PTY gew�nscht war */
	  if(want_read_ptym) {
	    if(got_sigchld_flag)
	      ptym_at_eof = 1;
	  }
	/* Test, ob Lesen der Standardeingabe gew�nscht war */
	  if(want_read_stdin) {
	    if(carrier_is_lost)
	      stdin_at_eof = 1;
	  }

    } /* while(1) */

  /* hierher kommen wir, wenn ein EOF vorliegt oder nach einem Carrier Lost */
  if(carrier_is_lost)
    {
      /* int sig=SIGHUP; */
      /* Signal an child */
      /*      if(-1==ioctl(ptym,TIOCSIGNAL,&sig))
	      err_sys("error signaling pty slave");
	      */

      /* Buffer flushen */
      if(! ptym_at_eof && isatty(ptym) && -1 == tcflush(ptym,TCOFLUSH))
	err_sys("error flushing pty");
      if(isatty(STDOUT_FILENO) && -1 == tcflush(STDOUT_FILENO,TCOFLUSH))
	err_sys("error flushing STDOUT");
    }
  /* fertig */
}







