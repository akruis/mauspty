#include	<sys/types.h>
#include	<signal.h>
#include	"ourhdr.h"

#define s_pipe(a) pipe(a)

void
do_driver(char *driver)
{
	pid_t	child;
	int		pipefd[2];

		/* create a stream pipe to communicate with the driver */
	if (s_pipe(pipefd) < 0)
		err_sys("can't create stream pipe");

	if ( (child = fork()) < 0)
		err_sys("fork error");

	else if (child == 0) {			/* child */
		close(pipefd[1]);

				/* stdin for driver */
		if (dup2(pipefd[0], STDIN_FILENO) != STDIN_FILENO)
			err_sys("dup2 error to stdin");

				/* stdout for driver */
		if (dup2(pipefd[0], STDOUT_FILENO) != STDOUT_FILENO)
			err_sys("dup2 error to stdout");
		close(pipefd[0]);

				/* leave stderr for driver alone */

		/* turn off Permissions */
		setuid(getuid());
		setgid(getgid());
		execlp(driver, driver, (char *) 0);
		err_sys("execlp error for: %s", driver);
	}

	close(pipefd[0]);		/* parent */

	if (dup2(pipefd[1], STDIN_FILENO) != STDIN_FILENO)
		err_sys("dup2 error to stdin");

	if (dup2(pipefd[1], STDOUT_FILENO) != STDOUT_FILENO)
		err_sys("dup2 error to stdout");
	close(pipefd[1]);

	/* Parent returns, but with stdin and stdout connected
	   to the driver. */
}

