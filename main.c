#include	<sys/types.h>
#include        <sys/stat.h>
#include	<termios.h>
#include	<sys/ioctl.h>	/* 44BSD requires this too */
#include        <sys/wait.h>
#include        <signal.h>
#include        <fcntl.h>
#include        <pty.h>
#include	"ourhdr.h"
#include        "extern.h"


void		do_driver(char *);	/* in the file driver.c */
/* in the file loop.c */
void            sig_chld(int signr );
int		loop(int fd, int ignoreeof, int carrierwatch, int timeout, int pty_packetmode);		

/* in the tcpopen.c */
int tcp_open(char * host,char * service,int port);
 

static void	set_noecho(int);	/* at the end of this file */
void            setup_env(int loginenv);/* at the end of this file */
void restore_modem_tty(void); /* at the end of this file */
void usage(void); /* at the end of this file */

/* Definitionen f�r das Environment */
#define EXTERN_LINES 50 /* Maximal 50 Zeilen extern.cfg */
#define STD_ENV_LINES 10 /* Anzahl der Standardenviromentzeilen */

#define ENVN (EXTERN_LINES + STD_ENV_LINES)
char	**env;
int     envcnt = 0;

static struct extern_cfg ecfg; /* Einstellungen aus der Extern.cfg */


/* the euid and the egid */
#ifdef ENABLE_SETEUID
uid_t euid;
gid_t egid;
#endif

/* Einstellungen des Modem-ttys */
static struct termios orig_modem_termios;

int
main(int argc, char *argv[])
{
  int	fdm, c, ignoreeof, interactive, noecho, verbose;
  int   waitchild, checkcarrier,timeout,modemtty,loginenv;
  int   envneed, pty_packetmode;
  unsigned int waittime;
  pid_t			pid;
  char			*driver,*extname;
  struct termios	orig_termios;
  struct winsize	size;
  int  child_stat; /* terminations-status des childs */
  
  char * host;
  char * port;

#ifdef ENABLE_SETEUID
  euid=geteuid(); /* save effective ids */
  egid=getegid();
  seteuid(getuid()); /* turn off permissions */
  setegid(getgid());
#endif

  interactive = isatty(STDIN_FILENO);
  ignoreeof = 0;
  noecho = 0;
  verbose = 0;
  driver = NULL;
  extname=NULL;
  waitchild=0;
  waittime=5;
  checkcarrier=0;
  timeout=0;
  modemtty=0;
  loginenv=0;
  host=NULL;
  port=NULL;
  pty_packetmode=0;
  
  /* Anzahl der Environmentvariablen z�hlen */
  for(envneed=0;NULL != environ[envneed];envneed++)
    ;
  if(NULL==(env = (char **) malloc((envneed+ENVN)*sizeof(char *))))
    err_sys("malloc");

  opterr = 0;		/* don't want getopt() writing to stderr */
  while ( (c = getopt(argc, argv, "d:cCeilMnt:vwW:x:hH:P:p")) != EOF) {
    switch (c) {
    case 'c':           /* check carrier */
      checkcarrier=1;
      break;

    case 'C':           /* disable carrier check */
      checkcarrier = -1;
      break;

    case 'd':		/* driver for stdin/stdout */
      driver = optarg;
      break;
      
    case 'e':		/* noecho for slave pty's line discipline */
      noecho = 1;
      break;
      
    case 'i':		/* ignore EOF on standard input */
      ignoreeof = 1;
      break;

    case 'l':           /* set Environment suitable for login */
      loginenv = 1;
      break;

    case 'M':           /* disable modem-tty */
      modemtty=-1;
      break;

    case 'n':		/* not interactive */
      interactive = 0;
      break;

    case 'p':           /* use pty packet mode */
      pty_packetmode = 1;
      break;
      
    case 't':
      if(1!=sscanf(optarg,"%d",&timeout))
	err_quit("invalid time_out value");
      break;

    case 'v':		/* verbose */
      verbose = 1;
      break;

    case 'w':           /* fetch the child status */
      waitchild=1;
      break;

    case 'W':           /* Seconds to wait for the termination of 
			   the child process. */
      if(1!=sscanf(optarg,"%u",&waittime) 
	 || !waittime || 360000 < waittime) /* 0 < waittime < 100 hours */
	err_quit("invalid waittime value");
      break;
      
    case 'x':            /* read maus extern.cfg */
      extname = optarg;
      break;
      
    case 'H':		/* TCP host */
	host = optarg;
	break;
	
    case 'P':		/* TCP port */
    	port = optarg;
    	break;
    	
    case 'h':
      usage();
      exit(EXIT_FAILURE);
      
    case '?':
      err_quit("unrecognized option: -%c", optopt);
    }
  }
  if (optind >= argc && !host)
    err_quit("mauspty: use -h for help");

  /* read extern.cfg */
  if(extname)
    {
      if(0==(envcnt=read_externcfg(extname,env,EXTERN_LINES,&ecfg)))
	err_sys("error: reading extern.cfg");

      if(!timeout)
	timeout=ecfg.timeleft;

      if(!checkcarrier)
	checkcarrier=ecfg.is_modem;
      
      if(modemtty!= -1 && *ecfg.tty)
	modemtty=1;
    }
  /* Install Signal-Handler for SIGCHLD */
  signal(SIGCHLD,sig_chld);  


  if(!host)
  {
#ifdef ENABLE_SETEUID
  /* Now turn on the permissions again, because we need to bee root for forkpty */
  seteuid(euid);
  setegid(egid);
#endif

  if (interactive || ecfg.used) 
    {	/* fetch current termios and window size */
      struct termios tt;

      if (tcgetattr(STDIN_FILENO, &orig_termios) < 0)
	err_sys("tcgetattr error on stdin");
      tt=orig_termios;
      if (ioctl(STDIN_FILENO, TIOCGWINSZ, (char *) &size) < 0)
	err_sys("TIOCGWINSZ error");

      if (ecfg.used)
	{
	  setup_modem_tty(&ecfg,&tt);
	  size.ws_row=ecfg.lines;
	  size.ws_col=ecfg.colums;
	}
      pid = forkpty(&fdm, NULL, &tt, &size);
    } 
  else
    pid = forkpty(&fdm, NULL, NULL, NULL);
  
  if (pid < 0)
    err_sys("fork error");
  
  else if (pid == 0) {	     /****************** child ******************/
#ifdef ENABLE_SETEUID
    /* turn off permissions */
    setuid(getuid());
    setgid(getgid());
#endif

    /* the environment */
    setup_env(loginenv);

    if (noecho)
      set_noecho(STDIN_FILENO);	/* stdin is slave pty */

    if (execvp(argv[optind], &argv[optind]) < 0)
      err_sys("can't execute: %s", argv[optind]);

    /*************************** end of child *********************/
  }
#ifdef ENABLE_SETEUID
  /* turn off permissions */
  seteuid(getuid());
  setegid(getgid());
#endif

  if (verbose) {
    if (driver != NULL)
      fprintf(stderr, "driver = %s\n", driver);
    if (modemtty==1)
      fprintf(stderr, "modem = %s\n", ecfg.tty);
  }
  
  
  } /* end of pseudo tty code */
  else 
  { /* TCP code */
    pid = -1; /* there is no child */
  
    if(!port)
     	err_quit("No port specified!");
 
    fdm=tcp_open(host,port,0);
  }

  if (interactive && driver == NULL && modemtty!=1) {
    if (tty_raw(STDIN_FILENO) < 0)	/* user's tty to raw mode */
      err_sys("tty_raw error");
    if (atexit(tty_atexit) < 0)		/* reset user's tty on exit */
      err_sys("atexit error: tty_atexeit");
  }

  if (1==modemtty) /* changes our stdin/stdout */
    {
      int fd; 
      if(-1==(fd=open(ecfg.tty,O_RDWR|O_NOCTTY|O_NONBLOCK)))
	err_sys("error: modem tty open");
      if(isatty(fd))
	{
	  struct termios tt;
	  if(-1 == tcgetattr(fd,&tt))
	    err_sys("error: tcgetattr (modem tty)");
	  orig_modem_termios = tt;
	  if (atexit(restore_modem_tty) < 0)
	    err_sys("atexit error: restore_modem_tty");

	  cfmakeraw(&tt);
	  tt.c_cflag |= (CLOCAL|CREAD|CRTSCTS);
	  tt.c_cc[VMIN]=1;
	  tt.c_cc[VTIME]=0;
	  if(-1 == tcsetattr(fd,TCSANOW,&tt))
	    err_sys("error: tcsetattr (modem tty)");
	}

      if (dup2(fd, STDIN_FILENO) != STDIN_FILENO)
	err_sys("dup2 error to stdin (modem tty)");
      if (dup2(fd, STDOUT_FILENO) != STDOUT_FILENO)
	err_sys("dup2 error to stdout (modem tty)");
      close(fd);
    }
  
  if (driver)
    do_driver(driver);	/* changes our stdin/stdout */

  if(checkcarrier < 0) checkcarrier =0;
  if(timeout < 0) timeout =0;

  if(checkcarrier > 0 && !isamodem(STDIN_FILENO))
    err_sys("error: carrier check requested, but input is not a modem"); 

  loop(fdm, ignoreeof, checkcarrier,timeout, pty_packetmode);	/* copies stdin -> ptym, ptym -> stdout */

  close(fdm); /* Sendet ein SIGHUP an den Cild */
  
  /* Exitcode vom Child holen */
  if(waitchild &&  -1 != pid)
    {
      int i;
      for(i=0;i<waittime*2+10;i++) 
	{
	  pid_t ret;
	  
	  ret = waitpid(pid,&child_stat,WNOHANG);
	  if( -1== ret)
	    perror("waitpid error");
	  if(ret == pid) /* we've got it */
	    {
	      if(WIFEXITED(child_stat))
		exit(WEXITSTATUS(child_stat));
	      if(WIFSIGNALED(child_stat))
	        {
#if 0	        
	          fprintf(stderr,"Child signaled %d",(int)WTERMSIG(child_stat));
#endif
	          exit(180+WTERMSIG(child_stat));
	        }
	      if(WIFSTOPPED(child_stat))
	        {
#if 0
	          fprintf(stderr,"Child stopped %d",(int)WSTOPSIG(child_stat));
#endif
	          exit(220+WSTOPSIG(child_stat));
	        }
	       err_sys("waitpid: unknown status returned");
	    }
	  if(i==waittime)
	    kill(pid,SIGTERM);
	  if(i==waittime*2)
	    kill(pid,SIGKILL);
	  sleep(1);
	}
      exit(219); /* Use an unconventional value to show something wrong happened */
    }
  exit(0);
}

static void
set_noecho(int fd)		/* turn off echo (for slave pty) */
{
	struct termios	stermios;

	if (tcgetattr(fd, &stermios) < 0)
		err_sys("tcgetattr error");

	stermios.c_lflag &= ~(ECHO | ECHOE | ECHOK | ECHONL);
	stermios.c_oflag &= ~(ONLCR);
			/* also turn off NL to CR/NL mapping on output */

	if (tcsetattr(fd, TCSANOW, &stermios) < 0)
		err_sys("tcsetattr error");
}

/* restore the modemtty */
void restore_modem_tty(void)
{
  if(-1 == tcsetattr(STDOUT_FILENO,TCSANOW,&orig_modem_termios))
    err_sys("error: tcsetattr (restore modemtty)");
}



void setup_env(int loginenv)
{
  int i;

  static char env_term[20]="TERM=";

  if(ecfg.used)
    strcat(env_term,ecfg.term);
  else
    strcat(env_term,"dump");

  if(!loginenv)
    {
      for(i=0;environ[i];i++)
	{
	  if(ecfg.used && !memcmp(environ[i],"TERM=",5))
	    continue;
	  env[envcnt++]=environ[i];
	}
      env[envcnt]=NULL;
    }

  if(loginenv || ecfg.used)
    env[envcnt++] = env_term;
  env[envcnt]=NULL;

  environ=env;
}


/* Usage und Copyright */
void usage(void)
{
puts("mauspty: run a command with pseudo-tty line disciplin. \n"
"Copyright: A. Kruis 2015 <anselm@kruis.de>.\n"
"Copyright: A. Kruis 1996 <ak@m2.maus.de>.\n"
"The programm is based on the pty programm from W. Richard Stevens\n"
"(Advanced Programming in the UNIX Environment, Addison-Wesley 1992).\n"
"\n"
"usage: mauspty [options] [--] command [args ...]\n"
"options:\n"
"-c	: enable carrier watchdog\n"
"-C	: disable carrier watchdog. Used with -x option\n"
"-d driver: use driver for stdin/stdout. See W.R.Stevens APUE\n"
"-e	: turn off echo for slave pty's line discipline\n"
"-i	: ignore EOF on standard input\n"
"-l	: set environment suitable for /bin/login\n"
"-M	: disable automatic use of the modem-device for stdin/stdout with\n"
"	  the -x option\n"
"-n	: disable interactive mode. See W.R.Stevens APUE\n"
"-p	: enable pty packet mode. See man 4 tty_ioctl\n"
"-t sec	: set the timeout to sec seconds. sec == -1 disables timeout\n"
"-v	: verbose\n"
"-w	: wait for the termination of the child and return the termination\n"
"	  status of the child\n"
"-W sec : number of seconds befor sending a SIGTERM/SIGKILL to the child\n"
"-x extern.cfg : read the extern.cfg-file from the m7com.exe programm.\n"
"	  the slave baud-rate, the timeout and the stdin/stdout is set.\n"
"	  The carrier watchdog is enabled.\n"
"	  The EXTCFG_-environment variables are set.\n"
"	  Try 'mauspty -Mx extern.cfg -- env' for details.\n"
"-h	: Show this message.\n"
"-H host: don't run a command in a pseudo TTY, instead connect to HOST\n"
"-P port: port to be used together with the -H option\n"
"\n"
"Environment:\n"
"MAUS_COM_x=dev : set the device for COMx: to dev.\n"
);
}



