/* Extern.cfg der Maus einlesen und auswerten
Anselm Kruis, 20-Feb 96 (ak@m2.maus.de) */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <termios.h>
#include "extern.h"

#define MAX_EXTERN_LINE_LEN 255

#define WSP " \t\b\r"
#define ENV_PREFIX "EXTCFG_"
#define IS_TRUE(a) (!strcmp("TRUE",(a)) || !strcmp("true",(a)))

#if 0 /* Ist kommentar */
Einlesen der extern.cfg 
Syntax:
zeilen:= /* */ | [zeile] zeilen;
zeile:= [zeile1] [['\r']'\n'] ;
zeile1:= [ws] [name [ws] [[':']'=' [ws] [val1|val2]]]
ws:={' '|'\t'|'\b'|'\r'}
val1:='\''value'\'' [anything]
val2:=value
#endif
int read_externcfg(const char * extname, char *env[],int max_env, 
		   struct extern_cfg *ecfg)
{
  FILE * fp;
  size_t l,namel;
  char line[MAX_EXTERN_LINE_LEN];
  char eline[MAX_EXTERN_LINE_LEN+sizeof(ENV_PREFIX)];
  char * p,*val,*name;
  int i;

  /* ecfg initialisieren */
  memset(ecfg,0,sizeof(*ecfg));
  ecfg->colums=80;
  strcpy(ecfg->term,"dump");
  ecfg->used=1;

  /* File �ffnen */
  if(NULL != (fp=fopen(extname,"r")))
    {
      for(i=0;i<max_env-1 && fgets(line,sizeof(line),fp);)
	{
	  l=strlen(line);
	  p=line;
	  /* remove eol */
	  while(l && (line[l-1]=='\n' || line[l-1]=='\r'))
	    line[--l]='\0';
	  /* remove leading white space */
	  p+=strspn(line,WSP);
	  /* Name lesen */
	  if(0==(l=strcspn(p,WSP ":=;#'\"" )))
	    continue;
	  name=p;
	  namel=l;
	  p+=l;

	  /* '=' suchen */
	  p+=strspn(p,WSP ":");
	  if('=' != *p++)
	    continue;
	  /* 0-Byte an name anh�ngen */
	  name[namel]='\0';
	  /* WSP �berlesen */
	  p+=strspn(p,WSP);
	  /* test ob, val1 (mit '\'' oder val2 */
	  if('\''==*p)
	    {/* val1 */
	      val=++p;
	      if(NULL==(p=strchr(p,'\'')))
		continue; /* abschlie�endes '\'' fehlt */
	      *p='\0';
	    }
	  else
	    val=p; /* rest der Zeile bei val2 */
	  
	  /* ok, now we've got a name and a value.
	     Check, wether the name is a special name */
	  if(!strcmp("ANSI",name))
	    {
	      if(IS_TRUE(val))
		strcpy(ecfg->term,"vt100");
	    }
	  else if(!strcmp("Zeilenzahl",name))
	    {
	      sscanf(val,"%d",&(ecfg->lines));
	    }
	  else if(!strcmp("RestZeit",name))
	    {
	      if(1==sscanf(val,"%d",&(ecfg->timeleft)))
		ecfg->timeleft*=60;
	      else
		ecfg->timeleft=0;
	    }
	  else if(!strcmp("BaudToUser",name))
	    {
	      sscanf(val,"%d",&(ecfg->baud));
	    }
	  else if(!strcmp("ComPort",name))
	    {
	      strcpy(eline,"MAUS_COM_");
	      strcat(eline,val);
	      if(NULL!=(p=getenv(eline)))
		strcpy(ecfg->tty,p);
	      { /* Test, ob modem */
		int fd;
		ecfg->is_modem= (0<(fd=open(ecfg->tty,O_RDWR|O_NOCTTY|O_NONBLOCK)) && isamodem(fd));
		close(fd);
	      }
	    }
	  /* place the Name in the environment */
	  sprintf(eline,ENV_PREFIX "%s=%s",name,val);
	  if(NULL==(env[i++]=strdup(eline)))
	    return 0;
	}
      env[i]=NULL;
      if(!ferror(fp))
	return i;
    }
  return 0;
}


/* Test, ob es sich um ein Modem handelt */
int isamodem(int fd) 
{
  int dummy;

  return isatty(fd) && (0==ioctl(fd,TIOCMGET,&dummy));
}


/* cfsetspeed.c - emulate BSD cfsetspeed with cfset[io]speed - rick sladkey */

static const struct {
	int flag, val;
} xref[] = {
	{B9600,	0},
	{B50,	50},
	{B75,	75},
	{B110,	110},
	{B134,	134},
	{B150,	150},
	{B200,	200},
	{B300,	300},
	{B600,	600},
	{B1200,	1200},
	{B1800,	1800},
	{B2400,	2400},
	{B4800,	4800},
	{B9600,	9600},
	{B19200,	19200},
	{B38400,	38400},
#ifdef B57600
	{B57600, 57600},
#endif
#ifdef B115200
	{B115200,115200},
#endif
#ifdef B230400
	{B230400,230400},
#endif
	{0,	-1}
};



/* Slave pty f�r dem Modem-Betrieb konfigurieren:
Baudrate setzen
Fenstergr��e setzen
Inputtranslation:  cr->lf
Outputtranslation: lf->crlf
*/
void setup_modem_tty(struct extern_cfg *ecfg,struct termios *t)
{
  int i;

  t->c_iflag &= ~( INPCK | ISTRIP | IGNCR | INLCR | IXOFF | IXON | IXANY);
  t->c_iflag |=  ( IGNBRK | ICRNL );

  /* t->c_oflag &= ~( ONOEOT ); */
  t->c_oflag |= ( OPOST | ONLCR /* |OXTABS */ );

  t->c_cflag &= ~( CSTOPB | PARENB | CSIZE);
  t->c_cflag |= ( CLOCAL | HUPCL | CREAD | CS8 | CRTSCTS );

  t->c_lflag &= ~( ECHONL );
  t->c_lflag |= ( ICANON | ECHO | ECHOE | ECHOK | ISIG | IEXTEN | ECHOKE | TOSTOP );

  for (i = 0; xref[i].val != -1; i++)
    ;
  for(i--; i ; i--) 
    {
      if (xref[i].val <= ecfg->baud) 
	{
	  cfsetispeed(t,xref[i].flag);
	  cfsetospeed(t,xref[i].flag);
	  break;
	}
    }
}


