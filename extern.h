/* Header File zum extern.cfg */

#ifndef EXTERN_C_HEADER
#define EXTERN_C_HEADER

struct extern_cfg {
  int  used;     /* 0, nicht initialisiert, 1 = initalisiert */
  char term[12]; /* name des terminals */
  int  lines;    /* Bildschirmzeilen */
  int  colums;   /* Spalten */
  int  timeleft; /* verbleibende Zeit in Sekunden */
  int  baud;     /* Baudrate zum Benutzer */
  char tty[255]; /* name des terminals, "" wenn nicht gefunden  */
  int  is_modem; /* 1, wenn ioctl(,TIOCMGET,) unterstützt wird */ 
};

extern int read_externcfg(const char * name, char *env[],int max_env, 
		   struct extern_cfg *ecfg);

extern void setup_modem_tty(struct extern_cfg *ecfg,struct termios *t);


extern int isamodem(int fd);



#endif /* #ifndef EXTERN_C_HEADER */



