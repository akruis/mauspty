# Common make definitions for GNU make under 386BSD.

# Definitions required in all program directories to compile and link
# C programs using gcc.  We also have to get rid of Sun's "-target" options,
# as gcc doesn't grok it.

CC=cc
LINK.c=$(CC) $(CFLAGS) $(CPPFLAGS) $(LDFLAGS)

# Common definitions

# CFLAGS	=  -Wall -g -D_GNU_SOURCE
CFLAGS	=  -Wall -O3 -g -D_GNU_SOURCE
# We don't normally use -O with gcc on the sparc.


TEMPFILES = core core.* *.o temp.* *.out typescript* *~
# Common temp files to delete from each directory.

PROGS = mauspty

all:	${PROGS}

mauspty:	main.o loop.o driver.o extern.o \
		error.o ttymodes.o setfl.o tcpopen.o hosterror.o

		${LINK.c} -o mauspty main.o loop.o driver.o extern.o \
		error.o ttymodes.o setfl.o \
		tcpopen.o hosterror.o -lutil ${LDLIBS}

clean:
	rm -f ${PROGS} ${TEMPFILES}

install:	mauspty
	install -o root -g root -m 4755 mauspty /usr/local/bin
