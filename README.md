# README #

This utility program is based on the source of the program "pty" published in Richard Stevens "Advanced Programming in the UNIX Environment" (APUE).

## Disclaimer ##

From the APUE souce code archive:

```
	LIMITS OF LIABILITY AND DISCLAIMER OF WARRANTY

The author and publisher of the book "Advanced Programming in the
Unix Environment" have used their best efforts in preparing this
software.  These efforts include the development, research, and
testing of the theories and programs to determine their effectiveness.
The author and publisher make no warranty of any kind, express or
implied, with regard to these programs or the documentation contained
in the book. The author and publisher shall not be liable in any event
for incidental or consequential damages in connection with, or arising
out of, the furnishing, performance, or use of these programs.
```

## Building ##

Run ```make```.

## Usage ##

Run ```mauspty --help``` to show the online help and read the pseudo-TTY chapter
of APUE.   

## Changelog

2015-04-04 release v0.8

- Move to git and publish the repo on bitbucket. 
- replace our forkpty() implementation by the function provided by libutil
- add flag -p to work around a long standing linux PTY bug:
Linux resets the packet mode on the PTY master on closing the last file
descriptor of the PTY slave. See https://lkml.org/lkml/2013/1/15/641
- Fix the packte mode event logic.

Older changes from the obsolete German file VERSION:

* v0.7: 2000-01-29 2 Warnungen ausgebaut, TCP Option eingebaut
* v0.6: 1998-09-28 Angepasst an glibc
* v0.5: 1998-01-13 Vielleicht stimmt die CL-Behandlung jetzt. Eigentlich müsste das ganze Programm mal neu geschrieben werden.
* v0.4: 1998-01-13 Signalbehandlung für SIGCHLD hinzugefügt. Bei Carrier Lost wird dem Child-Prozess erst ein SIGHUP geschickt, wenn er alle Daten gelesen hat. Race Condition in loop-Schleife beseitigt.
* v0.3: 1997-02-07 Option -W hienzugefügt.
* v0.2: 1996-12-21 Kleiner Bug bei waitpid
* v0.1: 1996 erste Version
